import 'package:tegrity/models/point.dart';

class Task {
  String taskID = "";
  String status = "";
  String uid = "";
  String type = "";
  List<String> paramValues = [];
  Map<String, Point> taskProgress = {};

  Task.fromMap(Map<String, dynamic> map) {
    taskID = map['taskID'];
    status = map['status'];
    uid = map['uid'];
    type = map['type'];
    paramValues = map['paramValues'];
    taskProgress = map['taskProgress'];
  }
}
