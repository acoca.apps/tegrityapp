import 'package:flutter/cupertino.dart';

class UserData {
  String? authUID;
  String? uid;
  String? name;
  String? email;
  bool secondAuthVerified = false;
  String? verificationImagePath;

  UserData({
    @required this.authUID,
    @required this.name,
    @required this.email,
  });

  UserData.fromMap(Map<String, dynamic> map) {
    authUID = map['authUID'];
    uid = map['uid'];
    name = map['name'];
    email = map['email'];
    secondAuthVerified = map['secondAuthVerified'];
    verificationImagePath = map['verificationImagePath'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "authUID": authUID,
      "uid": uid,
      "name": name,
      "email": email,
      "secondAuthVerified": secondAuthVerified,
      "verificationImagePath": verificationImagePath,
    };
    return map;
  }
}
