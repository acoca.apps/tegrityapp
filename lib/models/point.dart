class Point {
  double x = 0;
  double y = 0;
  bool verified = false;
  double radius = 0;

  Point({
    required this.x,
    required this.y,
    required this.verified,
    required this.radius,
  });
}
