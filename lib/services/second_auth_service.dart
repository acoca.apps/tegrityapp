import 'package:dio/dio.dart';

class SecondAuthService {
  Future getAuthPage(String uid) async {
    Map<String, dynamic> auth = {
      "api_key": "0d98d58d56b64ac2bc6888358adccf49",
      "secret_key": "2fd0cd3d-4dc6-458c-b9dd-d57d9e675de4"
    };

    Map<String, dynamic> body = {
      "api_key": "0d98d58d56b64ac2bc6888358adccf49",
      "secret": "2fd0cd3d-4dc6-458c-b9dd-d57d9e675de4",
      "payment_page_uid": "",
      "expiry_datetime": "10",
      "more_info": "",
      //"refURL_success": "https://www.domain.com/success/",
      //"refURL_failure": "https://www.domain.com/failure/",
      "refURL_callback":
          "https://us-central1-rubbitcarwash.cloudfunctions.net/payment/success",
      "uid": uid,
      "authType": ["trial", "symbol"], //Default: mix between all
    };

    Dio dio = Dio();
    try {
      String url = "http://192.168.1.24:5000/api/authPages/generateLink";

      var response = await dio.post(url,
          data: body,
          options: Options(headers: {
            "Content-Type": "application/json",
            // "Authorization": jsonEncode(auth),
          }));
      if (response.data['success']) {
        String authPageID = response.data['data']['auth_page_id'];
        String pageLink = response.data['data']['auth_page_url'];
        return pageLink;
      } else {
        return response.data['message'];
      }
    } catch (e) {
      return Future.error(e);
    }
  }
}
