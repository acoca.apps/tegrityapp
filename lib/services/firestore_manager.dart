import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tegrity/models/user.dart';

class FirestoreManager {
  static final FirestoreManager instance = FirestoreManager._internal();
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  factory FirestoreManager() {
    return instance;
  }

  FirestoreManager._internal();

  Future<String> createUser(UserData user) async {
    try {
      DocumentReference<Map<String, dynamic>> doc =
          _firestore.collection("Users").doc();
      user.uid = doc.id;
      doc.set(user.toMap());
      return user.uid!;
    } catch (e) {
      return Future.error("Error creating the user");
    }
  }

  Future<String> updateUser(UserData user) async {
    try {
      await _firestore.collection("Users").doc(user.uid).update(user.toMap());
      return "Success";
    } catch (e) {
      return Future.error("Error updating the user");
    }
  }
}
