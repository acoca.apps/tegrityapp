import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';

class StorageManager {
  static final StorageManager instance = StorageManager._internal();

  factory StorageManager() {
    return instance;
  }

  StorageManager._internal();

  Future? uploadImageToFirebase(File image, String destination) async {
    try {
      Reference ref = FirebaseStorage.instance.ref(destination);
      TaskSnapshot task = await ref.putFile(image);
      String imgPath = task.ref.fullPath;
      return imgPath;
    } catch (e) {
      print('error occured');
    }
  }
}
