import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:http/http.dart' as http;
import 'package:tegrity/screens/home_page.dart';
import 'package:tegrity/utilities/constants.dart';
import 'package:tegrity/utilities/functions.dart';
import 'package:tegrity/widgets/canvas/task_canvas.dart';
import 'package:tegrity/widgets/task/start_button.dart';
import 'package:tegrity/widgets/task/task_symbols.dart';
import 'package:tegrity/widgets/toast.dart';
import 'package:wave_loading_indicator/wave_progress_widget.dart';

import '../models/point.dart';

class P2PVideo extends StatefulWidget {
  const P2PVideo({Key? key}) : super(key: key);

  @override
  _P2PVideoState createState() => _P2PVideoState();
}

class _P2PVideoState extends State<P2PVideo> {
  RTCPeerConnection? _peerConnection;
  final _localRenderer = RTCVideoRenderer();
  String generationID = "";
  TaskCanvas? _taskCanvas;
  MediaStream? _localStream;

  RTCDataChannelInit? _dataChannelDict;
  RTCDataChannel? _dataChannel;
  String transformType = "none";

  bool _inCalling = false;

  bool _loading = false;
  Map<String, bool> taskSymbolsMap = {};
  bool showSuccessGreen = false;
  bool faceAuthenticated = false;
  bool faceRecognitionIconAnimation = true;
  double _iconSize = 200.0;

  String task = "";
  String ipPortAddress = "192.168.1.24:55555";

  final GlobalKey<TaskCanvasState> _canvasKey = GlobalKey<TaskCanvasState>();

  @override
  void initState() {
    super.initState();
    _taskCanvas = TaskCanvas(
      key: _canvasKey,
    );
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    initLocalRenderers();
  }

  Future<void> initLocalRenderers() async {
    await _localRenderer.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: OrientationBuilder(
        builder: (context, orientation) {
          return Stack(
            children: [
              if (faceAuthenticated)
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Align(
                    alignment: faceRecognitionIconAnimation
                        ? Alignment.topCenter
                        : Alignment.topLeft,
                    child: AnimatedSize(
                      curve: Curves.easeIn,
                      duration: const Duration(seconds: 1),
                      child: Column(
                        children: [
                          Image.asset(
                            "assets/face_checked.png",
                            height: _iconSize,
                            width: _iconSize,
                          ),
                          Text(
                            FirebaseAuth.instance.currentUser!.displayName ??
                                "",
                            style:
                                TextStyle(color: Colors.white, fontSize: 16.0),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              (_inCalling && !faceAuthenticated && task == "")
                  ? Padding(
                      padding:
                          const EdgeInsets.only(top: 30.0, left: 20, right: 20),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          "assets/linkface.gif",
                          scale: 1.6,
                        ),
                      ),
                    )
                  : Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Text(
                            taskDescriptionMap[task]!,
                            textAlign: TextAlign.center,
                            style: kTaskTextStyle,
                          ),
                        ),
                      ),
                    ),
              getTaskView(),
              !_loading
                  ? RTCVideoView(
                      _localRenderer,
                      objectFit:
                          RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
                      // mirror: true,
                    )
                  : Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          WaveProgress(
                            borderSize: 3.0,
                            size: 180,
                            borderColor: Colors.white,
                            foregroundWaveColor: Colors.blue,
                            backgroundWaveColor: Colors.white,
                            progress: 40, // [0-100]
                            innerPadding:
                                10, // padding between border and waves
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, bottom: 20.0),
                            child: Text(
                              "Loading Task...",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0),
                            ),
                          ),
                        ],
                      ),
                    ),
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 90.0),
              //   child: Align(
              //     alignment: Alignment.bottomCenter,
              //     child: MyTextField(
              //       hint: "Enter IP:PORT",
              //       iconData: null,
              //       onChange: (input) {
              //         // ipPortAddress = input;
              //       },
              //     ),
              //   ),
              // ),
              _taskCanvas!,

              //Camera
              if (_inCalling)
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    onTap: _toggleCamera,
                    child: Container(
                      height: 50,
                      width: 50,
                      color: Colors.black26,
                      child: Center(
                        child: Icon(
                          Icons.cameraswitch,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                ),
              if (!_inCalling && !_loading)
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      "Press the Start button to get a task",
                      textAlign: TextAlign.center,
                      style: kPageTitlesTextStyle,
                    ),
                  ),
                ),

              StartButton(
                loading: _loading,
                inCalling: _inCalling,
                makeCall: _makeCall,
                stopCall: _stopCall,
              ),
              if (showSuccessGreen)
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xFF43EA6E).withOpacity(0.3),
                ),
            ],
          );
        },
      ),
    );
  }

  void _onTrack(RTCTrackEvent event) {
    if (event.track.kind == "video") {
      _localRenderer.srcObject = _localStream;
      _toggleCamera();
    }
  }

  void _onDataChannelState(RTCDataChannelState? state) {
    switch (state) {
      case RTCDataChannelState.RTCDataChannelClosed:
        print("Camera Closed!!!!!!!");
        break;
      case RTCDataChannelState.RTCDataChannelOpen:
        print("Camera Opened!!!!!!!");
        break;
      default:
        print("Data Channel State: $state");
    }
  }

  Future<bool> _waitForGatheringComplete(_) async {
    // print("WAITING FOR GATHERING COMPLETE");
    if (_peerConnection!.iceGatheringState ==
        RTCIceGatheringState.RTCIceGatheringStateComplete) {
      return true;
    } else {
      await Future.delayed(Duration(seconds: 1));
      return await _waitForGatheringComplete(_);
    }
  }

  void _toggleCamera() async {
    if (_localStream == null) throw Exception('Stream is not initialized');

    final videoTrack = _localStream!
        .getVideoTracks()
        .firstWhere((track) => track.kind == 'video');
    await Helper.switchCamera(videoTrack);
  }

  Future<void> _negotiateRemoteConnection() async {
    return _peerConnection!
        .createOffer()
        .then((offer) {
          return _peerConnection!.setLocalDescription(offer);
        })
        .then(_waitForGatheringComplete)
        .then((_) async {
          var des = await _peerConnection!.getLocalDescription();
          var headers = {
            'Content-Type': 'application/json',
          };
          var request = http.Request(
            'POST',
            Uri.parse(
                'http://$ipPortAddress/offer'), // CHANGE URL HERE TO LOCAL SERVER
          );
          request.body = json.encode(
            {
              "sdp": des!.sdp,
              "type": des.type,
              "user_id": FirebaseAuth.instance.currentUser!.uid,
              "width": MediaQuery.of(context).size.width,
              "height": 500,
            },
          );
          request.headers.addAll(headers);

          http.StreamedResponse response = await request.send();

          String data = "";
          // print(response);
          if (response.statusCode == 200) {
            data = await response.stream.bytesToString();
            var dataMap = json.decode(data);
            // print(dataMap);
            generationID = dataMap['generation_id'];

            await _peerConnection!.setRemoteDescription(
              RTCSessionDescription(
                dataMap["sdp"],
                dataMap["type"],
              ),
            );
          } else {
            // print(response.reasonPhrase);
          }
        });
  }

  void onMessage(RTCDataChannelMessage data) {
    print(data.text);
    var map = jsonDecode(data.text);
    if (map['type'] != task) {
      setState(() {
        task = map['type'];
      });
    }
    faceAuthenticated = map["user_confirmed"];
    if (faceRecognitionIconAnimation && faceAuthenticated) {
      Future.delayed(Duration(milliseconds: 300), () {
        setState(() {
          faceRecognitionIconAnimation = false;
          _iconSize = 50.0;
        });
      });
    }
    switch (map['type']) {
      case "trial":
        List<Point> taskPoints = [];
        List<Point> trialPoints = [];
        for (var pointData in map['param_values']) {
          trialPoints.add(Point(
              x: pointData[0].toDouble(),
              y: pointData[1].toDouble(),
              verified: false,
              radius: 15));
        }

        for (int i = 0; i < 3; i++) {
          taskPoints.add(Point(
              x: map['$i']['location'][0].toDouble(),
              y: map['$i']['location'][1].toDouble(),
              verified: map['$i']['status'],
              radius: map['$i']['radius'].toDouble()));
        }
        _canvasKey.currentState!.setTaskPoints(taskPoints);
        _canvasKey.currentState!.setTrialPoints(trialPoints);
        break;
      case "symbol":
        Map<String, bool> symbolsMapFromServer =
            Map<String, bool>.from(map['param_values']);
        if (checkAllSymbolVerified(symbolsMapFromServer)) {
          _stopCall(changeFaceAuthenticated: false);
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (BuildContext context) => HomePage(task: "Symbol")),
              (Route<dynamic> route) => false);
        }
        if (!mapEquals(taskSymbolsMap, symbolsMapFromServer)) {
          showSuccessGreen = true;
          Future.delayed(Duration(milliseconds: 500), () {
            setState(() {
              showSuccessGreen = false;
            });
          });
          setState(() {
            taskSymbolsMap = symbolsMapFromServer;
          });
        }
        break;
    }
  }

  Future<void> _makeCall() async {
    setState(() {
      _loading = true;
    });
    var configuration = <String, dynamic>{
      'sdpSemantics': 'unified-plan',
    };

    //* Create Peer Connection
    if (_peerConnection != null) return;
    _peerConnection = await createPeerConnection(
      configuration,
    );

    _peerConnection!.onTrack = _onTrack;
    // _peerConnection!.onAddTrack = _onAddTrack;

    //* Create Data Channel
    _dataChannelDict = RTCDataChannelInit();
    _dataChannelDict!.ordered = true;
    _dataChannel = await _peerConnection!.createDataChannel(
      "chat",
      _dataChannelDict!,
    );

    _peerConnection!.onConnectionState = (state) {
      setState(() {});
    };
    _dataChannel!.onDataChannelState = _onDataChannelState;

    _dataChannel!.onMessage = (data) {
      onMessage(data);
    };

    final mediaConstraints = <String, dynamic>{
      'audio': false,
      'video': {
        'mandatory': {
          'minWidth': '500',
          'minHeight': '500',
          'minFrameRate': '7',
        },
        // 'facingMode': 'user',
        'facingMode': 'environment',
        'optional': [],
      }
    };

    try {
      var stream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
      // _mediaDevicesList = await navigator.mediaDevices.enumerateDevices();
      _localStream = stream;
      // _localRenderer.srcObject = _localStream;

      stream.getTracks().forEach((element) {
        _peerConnection!.addTrack(element, stream);
      });

      print("NEGOTIATE");
      await _negotiateRemoteConnection();

      Future.delayed(Duration(milliseconds: 1000), () {
        if (FirebaseAuth.instance.currentUser != null) {
          _dataChannel!.send(RTCDataChannelMessage(
              '{"type": "initChannel","generationID": "$generationID"}'));
        } else {
          _stopCall();
          makeToast("UID is not valid");
        }
      });
    } catch (e) {
      print(e.toString());
    }
    if (!mounted) return;

    setState(() {
      _inCalling = true;
      _loading = false;
    });

    Future.delayed(Duration(seconds: 60), () {
      if (!faceAuthenticated && task == "") {
        _stopCall();
        makeToast("Failed authenticate user face");
      }
    });
  }

  Future<void> _stopCall({bool changeFaceAuthenticated = true}) async {
    try {
      // await _localStream?.dispose();
      _canvasKey.currentState!.setTrialPoints([]);
      _canvasKey.currentState!.setTaskPoints([]);
      task = "";
      if (changeFaceAuthenticated) faceAuthenticated = false;
      await _dataChannel?.close();
      await _peerConnection?.close();
      _peerConnection = null;
      _localRenderer.srcObject = null;
      _iconSize = 200;
      faceRecognitionIconAnimation = true;
    } catch (e) {
      print(e.toString());
    }
    setState(() {
      _inCalling = false;
    });
  }

  Widget getTaskView() {
    switch (task) {
      case "":
        return Container();
      case "symbol":
        return Padding(
          padding: const EdgeInsets.only(top: 80.0),
          child: TaskSymbols(
            symbolsMap: taskSymbolsMap,
          ),
        );
      default:
        return Container();
    }
  }
}
