import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tegrity/screens/choose_api_example_page.dart';
import 'package:tegrity/screens/register.dart';
import 'package:tegrity/utilities/constants.dart';
import 'package:tegrity/widgets/common/my_text_field.dart';
import 'package:tegrity/widgets/common/rect_button.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'login_screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool showSpinner = false;
  final _auth = FirebaseAuth.instance;
  MyTextField? emailField;
  MyTextField? passwordField;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    initTextFields();
  }

  void initTextFields() {
    emailField = MyTextField(
      hint: "Email",
      iconData: Icons.email,
    );
    passwordField = MyTextField(
      hint: "Password",
      iconData: Icons.lock,
      isObscure: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xff181920),
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 70.0, bottom: 5.0),
                child: Text(
                  "Welcome Back!",
                  style: kPageTitlesTextStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 100.0),
                child: Text(
                  "Please sign in to your account",
                  style: kPageSubtitlesTextStyle,
                ),
              ),
              emailField!,
              Padding(
                padding: const EdgeInsets.only(top: 30.0, bottom: 15.0),
                child: passwordField!,
              ),

              Padding(
                padding: const EdgeInsets.only(top: 70.0, bottom: 20.0),
                child: RectButton(
                  title: "Sign In",
                  onTap: () {
                    setState(() {
                      showSpinner = true;
                    });
                    login();
                  },
                ),
              ),
              //The client has no account, passing him to the registration page.
              GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return RegistrationScreen();
                  }));
                },
                child: Text(
                  "Don't have an Account? Sign Up",
                  style: kScreenLinkTextStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void login() async {
    String email = emailField!.getFieldData();
    String password = passwordField!.getFieldData();
    try {
      //Checking if the user entered correct email and password with firebase auth.
      final user = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      //Correct
      if (user != null) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => ChooseApiExamplePage()),
            (Route<dynamic> route) => false);
      }
    } catch (e) {
      //Incorrect
      Fluttertoast.showToast(
          msg: "Wrong email or password",
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } finally {
      setState(() {
        showSpinner = false;
      });
    }
  }
}
