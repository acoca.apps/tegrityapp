import 'package:camera/camera.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tegrity/models/user.dart';
import 'package:tegrity/screens/add_image_screen.dart';
import 'package:tegrity/services/firestore_manager.dart';
import 'package:tegrity/utilities/constants.dart';
import 'package:tegrity/widgets/common/my_text_field.dart';
import 'package:tegrity/widgets/common/rect_button.dart';

class RegistrationScreen extends StatefulWidget {
  static const String id = 'registration_screen';
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _auth = FirebaseAuth.instance;
  bool showSpinner = false;
  MyTextField? nameField;
  MyTextField? emailField;
  MyTextField? passwordField;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    initTextFields();
  }

  void initTextFields() {
    nameField = MyTextField(
      hint: "Full Name",
      iconData: Icons.person,
    );
    emailField = MyTextField(
      hint: "Email",
      iconData: Icons.email,
    );
    passwordField = MyTextField(
      hint: "Password",
      iconData: Icons.lock,
      isObscure: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: bgColor,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 70.0, bottom: 5.0),
                child: Text(
                  "Create new account",
                  style: kPageTitlesTextStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 100.0),
                child: Text(
                  "Please fill in the form to continue",
                  style: kPageSubtitlesTextStyle,
                ),
              ),
              nameField!,
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30.0),
                child: emailField!,
              ),
              passwordField!,
              Padding(
                padding: const EdgeInsets.only(top: 50.0, bottom: 20.0),
                child: RectButton(
                  title: "Sign up",
                  onTap: () {
                    setState(() {
                      showSpinner = true;
                    });
                    register();
                  },
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "Have an Account? Sign In",
                  style: kScreenLinkTextStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void register() async {
    String name = nameField!.getFieldData();
    String email = emailField!.getFieldData();
    String password = passwordField!.getFieldData();
    try {
      final newUser = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      await newUser.user?.updateDisplayName(name);
      UserData user =
          UserData(name: name, email: email, authUID: newUser.user!.uid);
      String uid = await FirestoreManager().createUser(user);
      user.uid = uid;
      var cameras = await availableCameras();
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (BuildContext context) => AddImageScreen(
                    cameras: cameras,
                    userData: user,
                  )),
          (Route<dynamic> route) => false);
    } catch (e) {
      Fluttertoast.showToast(
          msg: "Please fill all the fields",
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } finally {
      setState(() {
        showSpinner = false;
      });
    }
  }
}
