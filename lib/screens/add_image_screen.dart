import 'dart:io';

import 'package:camera/camera.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tegrity/models/user.dart';
import 'package:tegrity/services/firestore_manager.dart';
import 'package:tegrity/services/p2p_video.dart';
import 'package:tegrity/services/storage_manager.dart';
import 'package:tegrity/widgets/common/confirm_button.dart';

class AddImageScreen extends StatefulWidget {
  const AddImageScreen({
    Key? key,
    required this.cameras,
    required this.userData,
  }) : super(key: key);

  final List<CameraDescription> cameras;
  final UserData userData;

  @override
  _AddImageScreenState createState() => _AddImageScreenState();
}

class _AddImageScreenState extends State<AddImageScreen> {
  late CameraController _controller; //To control the camera
  late Future<void>
      _initializeControllerFuture; //Future to wait until camera initializes
  int selectedCamera = 1;
  File? capturedImage;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    initializeCamera(selectedCamera); //Initially selectedCamera = 0
  }

  initializeCamera(int cameraIndex) async {
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.cameras[cameraIndex],
      // Define the resolution to use.
      ResolutionPreset.veryHigh,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          if (capturedImage == null)
            Column(
              children: [
                FutureBuilder<void>(
                  future: _initializeControllerFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      // If the Future is complete, display the preview.
                      return CameraPreview(
                        _controller,
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 30.0),
                            child: Text(
                              "Please take a clear picture of your face",
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 24.0,
                                fontFamily: 'IBMPlexSansHebrew',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                      );
                    } else {
                      // Otherwise, display a loading indicator.
                      return const Center(child: CircularProgressIndicator());
                    }
                  },
                ),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: () {
                          if (widget.cameras.length > 1) {
                            setState(() {
                              selectedCamera = selectedCamera == 0 ? 1 : 0;
                              initializeCamera(selectedCamera);
                            });
                          } else {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              content: Text('לא נמצא מצלמה שנייה'),
                              duration: Duration(seconds: 2),
                            ));
                          }
                        },
                        icon: Icon(Icons.switch_camera_rounded,
                            color: Colors.white),
                      ),
                      GestureDetector(
                        onTap: capture,
                        child: Container(
                          height: 60,
                          width: 60,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                          ),
                          child: const Icon(
                            Icons.camera,
                            size: 50,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 60,
                        width: 60,
                      ),
                    ],
                  ),
                ),
                Spacer(),
              ],
            ),
          if (capturedImage != null)
            Center(
              child: Container(
                height: 600,
                width: MediaQuery.of(context).size.width * 0.95,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                      image: FileImage(capturedImage!), fit: BoxFit.cover),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 8.0, right: 8.0, bottom: 20.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ConfirmButton(
                          title: 'Retake',
                          onTap: () {
                            setState(() {
                              capturedImage = null;
                            });
                          },
                          bgColor: Colors.redAccent,
                        ),
                        ConfirmButton(
                          title: 'Confirm',
                          onTap: confirmImage,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  void capture() async {
    if (_controller != null) {
      await _initializeControllerFuture;
      XFile xFile = await _controller.takePicture();
      capturedImage = File(xFile.path);
      setState(() {});
    }
  }

  void confirmImage() async {
    String? uid = FirebaseAuth.instance.currentUser?.uid;
    final destination = 'userVerificationImages/$uid.jpeg';
    String imgPath = await StorageManager()
        .uploadImageToFirebase(capturedImage!, destination);
    widget.userData.verificationImagePath = imgPath;
    FirestoreManager().updateUser(widget.userData);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return P2PVideo();
    }));
  }
}
