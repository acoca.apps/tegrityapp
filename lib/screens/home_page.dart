import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tegrity/screens/login.dart';
import 'package:tegrity/utilities/constants.dart';
import 'package:tegrity/widgets/common/rect_button.dart';

class HomePage extends StatefulWidget {
  HomePage({required this.task});

  String task;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _initialized = false;
  bool _error = false;
  FirebaseAuth? _auth;

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    initializeFlutterFire();
    super.initState();
  }

  // Define an async function to initialize FlutterFire
  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      _auth = FirebaseAuth.instance;
      checkAuth();
      setState(() {
        _initialized = true;
      });
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  //Moving the user to the login screen (if not logged in or clicking logout)
  void goToLoginScreen() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => LoginScreen()),
        (Route<dynamic> route) => false);
  }

  //Checking if the user already logged in (in this particular phone).
  void checkAuth() async {
    try {
      final user = _auth!.currentUser;
      if (user != null) {
      } else {
        //Not logged, go to the login screen.
        goToLoginScreen();
      }
    } catch (e) {
      goToLoginScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_error) {
      return Container();
    }

    //The Firebase isn't initialized yet.
    if (!_initialized) {
      return Container();
    }

    return Material(
      color: bgColor,
      child: Stack(
        children: [
          Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Color(0xFF5568FE).withOpacity(0.2),
                  blurRadius: 200,
                  spreadRadius: 1,
                  offset: const Offset(1, 4),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xFF5568FE).withOpacity(0.2),
                    blurRadius: 200,
                    spreadRadius: 1,
                    offset: const Offset(1, 4),
                  ),
                ],
              ),
            ),
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 80.0),
                child: Image.asset(
                  "assets/checked.png",
                  width: 80.0,
                  height: 80.0,
                  fit: BoxFit.contain,
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 30.0, bottom: 20.0),
                  child: Text(
                    "User Authenticated",
                    style: TextStyle(
                      fontSize: 35,
                      color: Colors.greenAccent,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Text(
                "Mission:",
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.grey,
                ),
              ),
              Text(
                widget.task,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 25.0, bottom: 120.0),
                child: Text(
                  "That is our new Authentication system, you can validate your"
                  " users identity in a simple and secure way.\nIf you are a bank, "
                  "owner of dating app or just someone that want a secure authentication system "
                  "that is just the product for you 🎃😉",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
              RectButton(title: "Sign Out And Try Again", onTap: logOut),
            ],
          ),
        ],
      ),
    );
  }

  void logOut() {
    _auth!.signOut();
    goToLoginScreen();
  }
}
