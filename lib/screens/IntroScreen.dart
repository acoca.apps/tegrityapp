import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tegrity/screens/home_page.dart';
import 'package:tegrity/widgets/intro/intro_slider.dart';
import 'package:tegrity/widgets/intro/slide_object.dart';

class IntroScreen extends StatefulWidget {
  IntroScreen({Key? key}) : super(key: key);

  @override
  IntroScreenState createState() => new IntroScreenState();
}

//------------------ Custom your own tabs ------------------
class IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = [];

  Function? goToTab;

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Application",
        styleTitle: TextStyle(
            color: Colors.white,
            fontSize: 40.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'RobotoMono'),
        description:
            "That is a sample app to demonstrate our Authentication method",
        styleDescription: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontStyle: FontStyle.italic,
            fontFamily: 'Raleway'),
        pathImage: "assets/intro/iphone.png",
        heightImage: 300,
        colorBegin: Colors.black,
        colorEnd: Color(0xFF86CFD9),
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomRight,
      ),
    );
    slides.add(
      new Slide(
        title: "Authentication",
        styleTitle: TextStyle(
            color: Colors.white,
            fontSize: 40.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'RobotoMono'),
        description:
            "Authentication using innovative face recognition technology and motion detection",
        styleDescription: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontStyle: FontStyle.italic,
            fontFamily: 'Raleway'),
        pathImage: "assets/intro/face_gif.gif",
        heightImage: 250,
        widthImage: 250,
        colorBegin: Colors.black,
        colorEnd: Colors.black,
        directionColorBegin: Alignment.topRight,
        directionColorEnd: Alignment.bottomLeft,
      ),
    );
    slides.add(
      new Slide(
        title: "Tegrity",
        styleTitle: TextStyle(
            color: Colors.white,
            fontSize: 40.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'RobotoMono'),
        description: "A safe platform for you to know your customer",
        styleDescription: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontStyle: FontStyle.italic,
            fontFamily: 'Raleway'),
        pathImage: "assets/intro/no_thief.png",
        heightImage: 250,
        widthImage: 250,
        colorBegin: Colors.black,
        colorEnd: Color(0xFF31AE84),
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomLeft,
      ),
    );
  }

  void onDonePress() {
    // Back to the first tab
    FirebaseAuth.instance.signOut();
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return HomePage(task: "Symbol");
    }));
  }

  void onSkipPress() {
    FirebaseAuth.instance.signOut();
    // Back to the first tab
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return HomePage(task: "Symbol");
    }));
  }

  void onTabChangeCompleted(index) {
    // Index of current tab is focused
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      color: Colors.black,
      size: 35.0,
    );
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Colors.black,
    );
  }

  Widget renderSkipBtn() {
    return Icon(
      Icons.skip_next,
      color: Colors.black,
    );
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = [];
    for (int i = 0; i < slides.length; i++) {
      Slide currentSlide = slides[i];
      tabs.add(Container(
        width: double.infinity,
        height: double.infinity,
        child: Container(
          margin: EdgeInsets.only(bottom: 60.0, top: 60.0),
          child: ListView(
            children: <Widget>[
              GestureDetector(
                  child: Image.asset(
                currentSlide.pathImage!,
                width: 200.0,
                height: 200.0,
                fit: BoxFit.contain,
              )),
              Container(
                child: Text(
                  currentSlide.title!,
                  style: currentSlide.styleTitle,
                  textAlign: TextAlign.center,
                ),
                margin: EdgeInsets.only(top: 20.0),
              ),
              Container(
                child: Text(
                  currentSlide.description!,
                  style: currentSlide.styleDescription,
                  textAlign: TextAlign.center,
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                ),
                margin: EdgeInsets.only(top: 20.0),
              ),
            ],
          ),
        ),
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      // List slides
      slides: this.slides,

      // Skip button
      renderSkipBtn: this.renderSkipBtn(),
      //colorSkipBtn: Colors.white70,
      //highlightColorSkipBtn: Colors.white,
      onSkipPress: this.onSkipPress,

      // Next button
      renderNextBtn: this.renderNextBtn(),

      // Done button
      renderDoneBtn: this.renderDoneBtn(),
      onDonePress: this.onDonePress,
      //colorDoneBtn: Colors.white70,
      //highlightColorDoneBtn: Colors.white,

      // Dot indicator
      colorDot: Colors.white70,
      colorActiveDot: Colors.white,
      sizeDot: 13.0,

      // Show or hide status bar
      //shouldHideStatusBar: true,
      backgroundColorAllSlides: Colors.grey,
    );
  }
}
