import 'package:flutter/material.dart';
import 'package:tegrity/screens/second_auth_screen.dart';
import 'package:tegrity/services/p2p_video.dart';
import 'package:tegrity/widgets/common/rect_button.dart';

class ChooseApiExamplePage extends StatefulWidget {
  const ChooseApiExamplePage({Key? key}) : super(key: key);

  @override
  State<ChooseApiExamplePage> createState() => _ChooseApiExamplePageState();
}

class _ChooseApiExamplePageState extends State<ChooseApiExamplePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xff181920),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(top: 40.0, left: 20.0, right: 20.0),
              child: Text(
                "Please choose Tegrity api example to continue with:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'migdal',
                  fontSize: 24.0,
                  color: Color(0xFFFFFFFF),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 60.0, top: 170.0),
              child: RectButton(
                title: "Web View Api Example",
                onTap: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              SecondAuthScreen()),
                      (Route<dynamic> route) => false);
                },
              ),
            ),
            RectButton(
              title: "Flutter implementation\nExample",
              onTap: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) => P2PVideo()),
                    (Route<dynamic> route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
