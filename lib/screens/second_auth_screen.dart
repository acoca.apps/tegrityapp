import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tegrity/services/second_auth_service.dart';
import 'package:webview_flutter/webview_flutter.dart';

class SecondAuthScreen extends StatefulWidget {
  const SecondAuthScreen({Key? key}) : super(key: key);

  @override
  State<SecondAuthScreen> createState() => _SecondAuthScreenState();
}

class _SecondAuthScreenState extends State<SecondAuthScreen> {
  Completer<WebViewController> _controller = Completer<WebViewController>();
  String? pageUrl;
  String? currentPageUrl;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    getUrl();
  }

  void getUrl() async {
    pageUrl =
        await SecondAuthService().getAuthPage("gH3TLIfigsMJ4jLsZ7ytz2i3dJA3");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (pageUrl == null) {
      return Container(
          color: Color(0xff181920),
          child: const Center(child: CircularProgressIndicator()));
    }
    return Material(
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              decoration: const BoxDecoration(
                color: Color(0xff181920),
              ),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 115,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment(-10, -20),
                        end: Alignment(2, 3),
                        stops: [
                          //0.1,
                          0.8,
                          1.0,
                        ],
                        colors: [
                          Color(0xFF01013F),
                          Color(0xFF0A0AB5),
                          //Color(0xFF01013F),
                        ],
                      ),
                    ),
                    child: Stack(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.09,
                              top: 30.49),
                          child: GestureDetector(
                            onTap: onBackClick,
                            child: const Icon(
                              Icons.arrow_back_ios_rounded,
                              size: 30.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: WebView(
                      initialUrl: pageUrl,
                      javascriptMode: JavascriptMode.unrestricted,
                      onWebViewCreated: (WebViewController webViewController) {
                        _controller.complete(webViewController);
                      },
                      onPageStarted: (String url) {
                        //TODO Check if new page is success page
                        if (url != pageUrl) {
                          // Navigator.of(context).pushAndRemoveUntil(
                          //     MaterialPageRoute(
                          //       builder: (BuildContext context) =>
                          //           OrderSuccessPage(
                          //         orderData: widget.orderData,
                          //       ),
                          //     ),
                          //     (Route<dynamic> route) => false);
                        }
                      },
                      onPageFinished: (String url) {},
                      //gestureNavigationEnabled: true,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onBackClick() {
    Navigator.pop(context);
  }
}
