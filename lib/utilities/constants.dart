import 'package:flutter/material.dart';

//Colors
Color bgColor = Color(0xff181920);

const kPageTitlesTextStyle = TextStyle(
  fontFamily: 'migdal',
  fontSize: 28.0,
  color: Color(0xFFFFFFFF),
  fontWeight: FontWeight.bold,
);

const kPageSubtitlesTextStyle = TextStyle(
  fontFamily: 'migdal',
  fontSize: 12.0,
  color: Color(0xFFC9C7C7),
);

const kTaskTextStyle = TextStyle(
  fontFamily: 'suez',
  fontSize: 18.0,
  color: Colors.white,
);

const kScreenLinkTextStyle = TextStyle(
  color: Color(0xFFC9C7C7),
  decoration: TextDecoration.underline,
  fontSize: 12.0,
);

const List<String> rtlLanguages = <String>[
  'ar', // Arabic
  'fa', // Farsi
  'he', // Hebrew
  'ps', // Pashto
  'ur', // Urdu
];

const Map<String, String> symbolEmojiMap = {
  "Ok": "👍",
  "Peace": "✌",
  "Question": "👆",
  "Hi": "✋",
  // "Fuck": "🖕",
  "Surfer": "🤙",
  "Loser": "assets/emoji/loser_emoji.png",
  "Pinky": "assets/emoji/pinky_emoji.png",
  "Rock": "🤘",
  "Fist": "✊",
};

const Map<String, String> taskDescriptionMap = {
  "": "",
  "trial": "Please touch all the points on the screen",
  "symbol": "Please make the following symbols by order",
};

// Scrollbar behavior values
enum scrollbarBehavior { HIDE, SHOW, SHOW_ALWAYS }
