bool checkAllSymbolVerified(Map<String, bool> map) {
  bool verified = true;
  for (bool success in map.values) {
    verified &= success;
  }
  return verified;
}
