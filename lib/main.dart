import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:tegrity/screens/IntroScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        canvasColor: Color(0xFF232323),
        primaryColor: Color(0xFF292929),
      ),
      home: IntroScreen(),
    );
  }
}
