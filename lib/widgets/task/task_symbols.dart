import 'package:flutter/material.dart';
import 'package:tegrity/utilities/constants.dart';

class TaskSymbols extends StatelessWidget {
  const TaskSymbols({Key? key, required this.symbolsMap}) : super(key: key);

  final Map<String, bool> symbolsMap;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (var pair in symbolsMap.entries)
          Column(
            children: [
              !symbolEmojiMap[pair.key]!.contains(".png")
                  ? Text(
                      symbolEmojiMap[pair.key]!,
                      style: TextStyle(fontSize: 40),
                    )
                  : Image.asset(
                      symbolEmojiMap[pair.key]!,
                      width: 50.0,
                      height: 50.0,
                      fit: BoxFit.contain,
                    ),
              if (pair.value)
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Icon(
                    Icons.check_circle,
                    color: Colors.green,
                    size: 25,
                  ),
                )
            ],
          ),
      ],
    );
  }
}
