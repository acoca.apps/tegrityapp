import 'package:flutter/material.dart';

class StartButton extends StatefulWidget {
  const StartButton({
    Key? key,
    required this.loading,
    required this.inCalling,
    required this.makeCall,
    required this.stopCall,
  }) : super(key: key);

  final bool loading;
  final bool inCalling;
  final VoidCallback makeCall;
  final VoidCallback stopCall;

  @override
  State<StartButton> createState() => _StartButtonState();
}

class _StartButtonState extends State<StartButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 30.0),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: InkWell(
          onTap: widget.loading
              ? () {}
              : widget.inCalling
                  ? widget.stopCall
                  : widget.makeCall,
          child: Container(
            decoration: BoxDecoration(
              color: widget.loading
                  ? Colors.amber
                  : widget.inCalling
                      ? Colors.red
                      : Colors.green,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: widget.loading
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircularProgressIndicator(),
                    )
                  : Text(
                      widget.inCalling ? "STOP" : "START",
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
