import 'package:flutter/material.dart';
import 'package:tegrity/models/point.dart';

class TaskCanvas extends StatefulWidget {
  TaskCanvas({Key? key}) : super(key: key);

  @override
  State<TaskCanvas> createState() => TaskCanvasState();
}

class TaskCanvasState extends State<TaskCanvas> {
  late ValueNotifier<List<Point>> trialNotifier;
  late ValueNotifier<List<Point>> taskNotifier;

  @override
  void initState() {
    super.initState();
    trialNotifier = ValueNotifier([]);
    taskNotifier = ValueNotifier([]);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.transparent,
        height: MediaQuery.of(context).size.height * 0.6,
        width: MediaQuery.of(context).size.width,
        child: CustomPaint(
          painter: TaskPainter(context, trialNotifier, taskNotifier),
        ),
      ),
    );
  }

  void setTaskPoints(List<Point> newPoints) {
    setState(() {
      taskNotifier.value = newPoints;
    });
  }

  void setTrialPoints(List<Point> newPoints) {
    setState(() {
      trialNotifier.value = newPoints;
    });
  }
}

class TaskPainter extends CustomPainter {
  ValueNotifier<List<Point>> taskNotifier;
  ValueNotifier<List<Point>> trialNotifier;

  TaskPainter(BuildContext context, this.trialNotifier, this.taskNotifier)
      : super(repaint: trialNotifier);

  @override
  void paint(Canvas canvas, Size size) {
    // Define a paint object

    for (Point point in taskNotifier.value) {
      final paint = Paint()
        ..style = PaintingStyle.fill
        ..color = point.verified ? Colors.greenAccent : Colors.red;

      canvas.drawOval(
        Rect.fromLTWH(point.x, point.y, 20, 20),
        paint,
      );
    }

    List<Offset> pathPoints = [];

    for (Point point in trialNotifier.value) {
      pathPoints.add(Offset(point.x, point.y));
    }

    Paint paint = Paint()
      ..color = Colors.blue
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5.0;

    Path path = Path();
    // Adds a polygon from the starting point to quarter point of the screen and lastly
    // it will be in the bottom middle. Close method will draw a line between start and end.
    path.addPolygon(pathPoints, false);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(TaskPainter oldDelegate) => true;
}
