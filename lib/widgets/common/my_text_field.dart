import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MyTextField extends StatelessWidget {
  MyTextField({
    @required this.hint,
    @required this.iconData,
    this.isObscure = false,
    this.onChange,
  });
  var _controller = TextEditingController();
  final String? hint;
  final IconData? iconData;
  String _fieldData = "";
  bool isObscure;
  Function(String)? onChange;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: TextField(
        controller: _controller,
        obscureText: isObscure,
        onChanged: (text) {
          _fieldData = text;
          if (onChange != null) {
            onChange!(text);
          }
        },
        textAlign: TextAlign.left,
        style: TextStyle(
          color: Colors.white,
          fontSize: 14.0,
        ),
        decoration: InputDecoration(
          prefixIcon: Icon(
            iconData,
            size: 30.0,
            color: Colors.grey[600],
          ),
          filled: true,
          fillColor: Color(0xFF252A34),
          hintText: hint,
          hintStyle: TextStyle(
            color: Colors.grey[600],
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
              borderSide: BorderSide.none),
        ),
      ),
    );
  }

  String getFieldData() {
    _controller.clear();
    return _fieldData;
  }
}
