import 'package:flutter/material.dart';

class RectButton extends StatelessWidget {
  const RectButton({
    Key? key,
    @required this.title,
    @required this.onTap,
  }) : super(key: key);

  final String? title;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 50,
          decoration: BoxDecoration(
            color: Color(0xFF5568FE),
            borderRadius: BorderRadius.circular(20),
          ),
          child: Center(
            child: Text(
              title!,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
