import 'package:flutter/material.dart';

class ConfirmButton extends StatelessWidget {
  ConfirmButton({
    @required this.title,
    @required this.onTap,
    this.bgColor = const Color(0xFF31AE84),
  });
  final String? title;
  final VoidCallback? onTap;
  final Color? bgColor;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap!,
      child: Container(
        height: 40.0,
        width: 151.0,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(30.0)),
          color: bgColor,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.25),
              blurRadius: 4,
              spreadRadius: 1,
              offset: const Offset(1, 4),
            ),
          ],
        ),
        child: Center(
          child: Text(
            title!,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );
  }
}
